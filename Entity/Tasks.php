<?php

namespace ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
/**
 * 
 *
 * @ORM\Entity
 * @ORM\Table(name="task")
 */
class Tasks
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", nullable=true,length=100)
     * 
     */
    protected $description;
    /**
     * @ORM\OneToMany(targetEntity="ShopBundle\Entity\Tags", cascade={"all"}, mappedBy="id_task", fetch="EAGER")
     */
    protected $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function addTag(Tags $tag)
    {
        $this->tags->add($tag);
    }

    public function removeTag(Tags $tag)
    {
        $this->tags->removeElement($tag);
    }

    public function __toString() {
        return $this->description;
    }
}