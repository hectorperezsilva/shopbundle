<?php

namespace ShopBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 
 * @ORM\Entity
 * @ORM\Table(name="tag")
 */
class Tags
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", nullable=true,length=100)
     */
    protected $name;
    /**
     * 
     * @ORM\ManyToOne(targetEntity="ShopBundle\Entity\Tasks", inversedBy="tags")
     * @ORM\JoinColumn(name="id_task", referencedColumnName="id")
     */
    protected $id_task;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getIdTask()
    {
        return $this->id_task;
    }

    public function setIdTask($id_task)
    {
        $this->id_task = $id_task;
    }
}