<?php

namespace ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * 
 *
 * @ORM\Entity
 * @ORM\Table(name="tag")
 */
class Tag
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="tags")
     * @ORM\Column(type="string", nullable=true,length=100)
     * 
     */
    private $tast;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->tast;
    }

    public function setName($tast)
    {
        $this->tast = $tast;
    }
}