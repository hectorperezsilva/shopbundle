<?php

namespace ShopBundle\services;
use ShopBundle\services\service;

class service2
{
    private $servicio;

    public function __construct(service $servicio){

        $this->servicio = $servicio;

    }

    public function mainService($nombre){

        return $this->servicio->getHappyMessage($nombre);

    }



}