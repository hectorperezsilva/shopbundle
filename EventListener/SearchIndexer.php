<?php

namespace ShopBundle\EventListener;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use ShopBundle\Controller\secondController;


use App\Entity\Employee;

class SearchIndexer
{
    /**
     * @var ControllerResolverInterface
     */
    protected $resolver;

    /**
     * DeliveryRequisitionListener constructor.
     * @param ControllerResolverInterface $resolver
     */
    public function __construct(ControllerResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }


    public function postPersist(GetResponseEvent $args)
    {
        //dump($args);        
    }

    public function control(FilterControllerEvent $event)
    {
        
    }

    public function view(GetResponseForControllerResultEvent $view)
    {
        //dump($view); 
    }

    public function response(FilterResponseEvent $rsp)
    {
        //dump($rsp); 
        //$response = $rsp->getResponse();
        //$response->setContent($response->getContent().'xxxx');
    }

    public function finish(FinishRequestEvent $fnsh)
    {
        //dump($fnsh); 
    }
    
    public function terminate(PostResponseEvent $tmte)
    {
        //dump($tmte); 
    }

    public function exception(GetResponseForExceptionEvent $excp)
    {
        //dump($excp); 
    }

}