<?php
 
namespace ShopBundle\Controller;

use App\Entity\Employee;
use App\Entity\Position;
use App\Entity\Office;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ShopBundle\services\service;
use ShopBundle\Entity\Tasks;
use ShopBundle\Entity\Tags;
use ShopBundle\Form\Type\TaskType;

class DefaultController extends Controller
{
    /**
     * @Route("/{name}")
     * @param $name
     * @return Response
     */
    public function shopAction(){

        $posts = $this->getDoctrine()->getRepository('App:Employee')->findAll();
        return $this->render("ShopBundle:default:shop.html.twig", ['posts' => $posts]);
       
    }
    public function mostrarAction($id, Request $request){

        $emp = $this->getDoctrine()->getRepository('App:Employee')->find($id);
        $em = $this->getDoctrine()->getManager();
        //dump($emp);

        $form = $this->createForm(\ShopBundle\Form\PostType::class, $emp);

           if ($request->getMethod() == 'POST') {
               //echo '<script>alert("a")</script>';
               $form->handleRequest($request);

               if ($form->isSubmitted() && $form->isValid()){

                $em->flush();
                //return $this->redirectToRoute('shop');
               }


           }


            return $this->render('ShopBundle:default:empleado.html.twig', array(
                'form' => $form->createView(),
            ));
            
       // $emp = $this->getDoctrine()->getRepository('App:Employee')->findBy(array('id' => $id));
       // return $this->render("ShopBundle:default:empleado.html.twig", ['emp' => $emp]);
       
    }
    public function editarAction($id, Request $request){

        if ($request->isMethod('POST')) {
            $name = $request->request->get('name');
            $lastName = $request->request->get('lastName');
            $secondLastName = $request->request->get('secondLastName');
            $rut = $request->request->get('rut');
            $phone = $request->request->get('phone');

            $em = $this->getDoctrine()->getManager();
            $id = $em->getRepository('App:Employee')->find($id);

            $id->setName($name);
            $id->setLastName($lastName);
            $id->setSecondLastName($secondLastName);
            $id->setRut($rut);
            $id->setPhone($phone);
            $em->flush();

            return $this->redirectToRoute('shop');
        }
        else{
            return $this->redirectToRoute('shop');
        }    
    }
  
    public function eliminarAction($id, Request $request){

        if ($request->isMethod('POST')) {
            
            $em = $this->getDoctrine()->getManager();
            $id = $em->getRepository('App:Employee')->find($id);
            
            $em->remove($id);
            $em->flush();

            return $this->redirectToRoute('shop');

        }
        else{
            return $this->redirectToRoute('shop');
        }
        

    }

    public function nuevoAction(Request $request){

        $employee = new Employee();
        

        $form = $this->createForm(\ShopBundle\Form\PostType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($employee);
            $em->flush();
            return $this->redirectToRoute('shop');
        }


        return $this->render('ShopBundle:default:nuevo.html.twig', array(
            'form' => $form->createView(),
        ));

        //return $this->render("ShopBundle:default:nuevo.html.twig");

    }

    public function crearAction(Request $request){

        if ($request->isMethod('POST')) {

            $name = $request->request->get('name');
            $lastName = $request->request->get('lastName');
            $secondLastName = $request->request->get('secondLastName');
            $rut = $request->request->get('rut');
            $phone = $request->request->get('phone');

            $em = $this->getDoctrine()->getManager();
            $id = new Employee();
            $id->setName($name);
            $id->setLastName($lastName);
            $id->setSecondLastName($secondLastName);
            $id->setRut($rut);
            $id->setPhone($phone);

            $em->persist($id);
            $em->flush();

            return new Response('Usuario Agregado Correctamentes');
            return $this->redirectToRoute('shop');


        }
        else{
            return $this->redirectToRoute('shop');
        }
    }

    public function serviceAction(){
        
        $nombre = 'Martín';
        $message2 = $this->get("app2.service");
        return $this->render("ShopBundle:default:service.html.twig", array('msg' => $message2->mainService($nombre)));
    
    }

    public function taskAction(Request $request)
    {
        /*$task = new Tasks();

        $tag1 = new Tags();
        $tag1->setName('tag1');
        $task->getTags()->add($tag1);
        $tag12 = new Tags();
        $tag12->setName('tag12');
        $task->getTags()->add($tag12);

        $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
        

        return $this->render('ShopBundle:default:task.html.twig');*/

        $task = new Tasks();

        $tag1 = new Tags();
        $tag1->setName('tag1');
        $task->getTags()->add($tag1);

        $form = $this->createForm(TaskType::class, $task)->add('save', SubmitType::class, array('label' => 'Enviar'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump($request);
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
        }

        return $this->render('ShopBundle:default:task.html.twig', array(
            'form' => $form->createView(),
        ));


       
    }
    public function taskListAction(){

        $posts = $this->getDoctrine()->getRepository('ShopBundle:Tasks')->findAll();
        return $this->render("ShopBundle:default:taskList.html.twig", ['posts' => $posts]);

    }

    public function vueAction(){
        return $this->render("ShopBundle:default:vue.html.twig");
    }
    
}