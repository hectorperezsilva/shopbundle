<?php
 
namespace ShopBundle\Form;

use App\Entity\Employee;
use App\Entity\Position;
use App\Entity\Office;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
 
class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', TextType::class)
        ->add('lastName', TextType::class)
        ->add('secondLastName', TextType::class)
        ->add('rut', TextType::class)
        ->add('user', EntityType::class, array(
         'class' => User::class,
          ))
        ->add('phone', TextType::class)
        ->add('position', EntityType::class, array(
         'class' => Position::class,
          ))
         ->add('office', EntityType::class, array(
          'class' => Office::class,
          ))
        ->add('save', SubmitType::class, array('label' => 'Enviar'))
        ;
    }
 
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Employee',
        ));
    }
}